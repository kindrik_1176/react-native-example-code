



import React from 'react';
import { Text, Image, View, StyleSheet } from 'react-native';
import { Router, Scene } from 'react-native-router-flux';
import { Provider } from "react-redux";
import store from "./store/index";
import login from './components/login.js';
import forgot_pass from './components/forgot_pass.js';
import createHero from './components/createHero.js';
import homepage from './components/homepage.js';
import availability from './components/availability.js';
import avail_2 from './components/avail_2.js';
import my_jobs from './components/my_jobs.js';
import payroll from './components/payroll.js';

function home_icon(props){
  return (
    <View>
      <Image source={require('./components/assets/homepage.png')} style={styles.home_icon} />
    </View>
  )
}

function availability_icon(props){
  return (
    <View>
      <Image source={require('./components/assets/availability_icon.png')} style={styles.home_icon} />
    </View>
  )
}

function jobs_icon(props){
  return (
    <View>
      <Image source={require('./components/assets/my_jobs.png')} style={styles.home_icon} />
    </View>
  )
}

function payroll_icon(props){
  return (
    <View>
      <Image source={require('./components/assets/payroll.png')} style={styles.home_icon} />
    </View>
  )
}

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Scene key='root'>

            <Scene key='login' component={ login } title='Login' initial hideNavBar={true}/>
            <Scene key='forgot_pass' component={ forgot_pass } title='Forgot Password' hideNavBar={true} />
            <Scene key='createHero' component={ createHero } title='Create Hero' hideNavBar={true} />

            <Scene key='tab_nav' tabs  style={styles.navbar}>
              <Scene key='homepage' component={ homepage } title='Home' initial icon={home_icon} hideNavBar={true} gestureEnabled={false} panHandlers={null} />
              <Scene key='availability' component={ avail_2 } title='Availability' icon={availability_icon} hideNavBar={true} />
              <Scene key='my_jobs' component={ my_jobs } title='My Jobs' icon={jobs_icon} hideNavBar={true} />
              <Scene key='payroll' component={ payroll } title='Payroll' icon={payroll_icon} hideNavBar={true} />
            </Scene>

          </Scene>
        </Router>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  icon:{
    marginTop:10,
    bottom:25,

  },
  home_icon:{
    marginTop:10,
    bottom:25
  },
  navbar:{
    backgroundColor:'#fff',
    borderTopWidth:1,
    borderColor:'#000',
    height:60
  }
});





var axios = require('axios');

import React, { Component } from 'react';
import { AppRegistry, StyleSheet, View, Image, AsyncStorage, ImageBackground, ScrollView,  TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Container, Header, Content, Footer, FooterTab, Button, Text, Item, Input, H2, Card, CardItem, Body } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import { Col, Row, Grid } from 'react-native-easy-grid';

export default class homepage extends Component {

    constructor(props){
        super(props)

        this.state = {

        }
    }

    componentDidMount(){
        AsyncStorage.getItem("sessionid").then((value) => {
            this.setState({sessionid:value});

            AsyncStorage.getItem("homepage_arn").then((value) => {
                this.setState({homepage_arn:value});

                axios.post(this.state.homepage_arn, {sessionid: this.state.sessionid})
                .then((response) =>{
                    this.setState({templates: response.data.response.result.slider});
                    this.setState({vertical: response.data.response.result.vertical});
                });

            }).done();

        }).done();
    }

    logout(){
        var terminate = '';
        AsyncStorage.setItem('sessionid', terminate);
        Actions.login();
    }

    render() {

        if (!this.state.templates || !this.state.vertical) {
            return null;
        } else {

            const templates = this.state.templates;

            return (
                <Container style={ styles.container }>
                        <Content style={ styles.content }>
                        
                            <Row>
                                <ImageBackground source={require('./assets/header_image.png')} style={{flex: 1,width: null,height: null,}} resizeMode="cover">
                                    <ScrollView horizontal pagingEnabled showsHorizontalScrollIndicator={false}>
                                            {this.state.templates.map((templates, index) =>(
                                                    <Col style={styles.col_background} key = {index}>
                                                        <Text style={styles.horizontalText_2} key={index}>Your current acceptance rate</Text>
                                                        <Text style={styles.horizontalText}>{templates.data.acceptencerate}%</Text>
                                                        <Text style={{textAlign:'center',fontSize:10, color:'white',paddingTop:10}}>You can improve this number by</Text>
                                                        <Text style={{textAlign:'center',fontSize:10, color:'white'}}>updating your availability</Text>
                                                        <View style={{ flex:1,flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
                                                        <Button bordered info style={{borderColor:'white', marginTop:10}}>
                                                            <Text style={{color:'white'}}  onPress={() => this.logout()}>Logout</Text>
                                                        </Button>
                                                        </View>
                                                    </Col>
                                            ))}
                                    </ScrollView>
                                </ImageBackground>
                            </Row>
                            <Row style={styles.alerts}>
                                <ScrollView>
                                    
                                        <Card>
                                            <CardItem>
                                                <Body style={{flex:1,justifyContent:'center', alignItems:'center'}}>
                                                    {this.state.vertical.map((templates, index) =>(
                                                        <View key = {index}>
                                                            <Text style={styles.verticalText}>{templates.template.toString()}</Text>
                                                            <Text style={styles.verticalText}>{templates.description.toString()}</Text>
                                                            <Text style={styles.verticalText}>Priority: {templates.priority.toString()}</Text>
                                                        </View>
                                                    ))}
                                                </Body>
                                            </CardItem>
                                        </Card>

                                </ScrollView>
                            </Row>
                                                
                        </Content>
                </Container>
            )
        }
    }
}

AppRegistry.registerComponent('homepage', () => homepage);

const styles = StyleSheet.create({
    container:{
        borderWidth:0,
    },
    content:{
        height:600,
    },
    slider:{
        height:200,
        borderColor:'black',
        borderWidth:1,

    },
    footer_text:{
        fontSize:12
    },
    image:{
        marginBottom:5
    },
    col_background:{
        height:200,
        width:320,
        padding:10
    },

    slider_text:{
        textAlign:'center'
    },
    horizontalText:{
        textAlign:'center',
        color:'#fff',
        fontSize:24,
        marginTop:10,
    },
    horizontalText_2:{
        textAlign:'center',
        color:'#fff',
        fontSize:14,
        marginTop:20,
    },
    verticalText:{
        textAlign:'center',
        color:'#000',
        fontSize:16,
    },
    alerts:{
        margin:10
    },
    logInGradient: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 5,
        marginTop:20
      },
    button:{
        height:40
    },
    buttonText:{
        marginTop:10,
        textAlign:'center',
        color:'white'
    },

})